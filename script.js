
var SVG_TO_LUA = ((params)=>{
    
    let dropzone = params.dropzone
    let canvasOriginal = params.canvasOriginal
    let canvasOriginal2 = params.canvasOriginal2
    let canvasPoints = params.canvasPoints
    let canvasLines = params.canvasLines
    let canvasTriangles = params.canvasTriangles
    let outputJavascript = params.outputJavascript
    let outputLua = params.outputLua
    let outputLuaStormworks = params.outputLuaStormworks
    let settingsContainer = params.settingsContainer
    let messageContainer = params.messageContainer

    $(window).on('load', init)

    $(window).on('resize', ()=>{
        resizeCanvas()
        resizeTextareas()
    })

    $(window).bind('drop dragover', (ev)=>{
        /*if(! $(ev.target).hasClass('dropzone')){
            console.log('prevent drop, dragover', ev.target)*/
            ev.preventDefault()
            ev.stopPropagation()
        //}
    })

    let svgLoader
    let currentSVG
    let currentSVGRatio
    let currentSVGWidth
    let currentSVGHeight
    let fileInput

    const settings = {
        points_per_svg_path: {
            type: 'number',
            default: 150,
            min: 3,
            max: 5000,
            label: 'Points',
            hint: 'higher = more triangles'},
        decimals_to_stay: {
            type: 'number',
            default: 2,
            min: 0,
            max: 10,
            label: 'Precision',
            hint: 'higher = more precise triangles'},
        max_chars_per_lua_script: {
            type: 'number',
            default: 4096,
            min: 800,
            label: 'Max Stormworks Lua Chars',
            hint: 'Maximum characters per lua script'
        },
        output_image_height: {
            type: 'number',
            default: 64,
            min: 1,
            label: 'Output Image Height',
            hint: 'on Stormworks Monitors'
        },
        color: {
            type: 'color',
            default: '#fff',
            label: 'Color',
            hint: 'The color of all triangles'
        }
    }

    function init(){
        fileInput = $('<input id="file-input" type="file" name="file-input" style="display: none"/>')
        dropzone.append(fileInput)

        dropzone.get(0).ondragover = (ev)=>{
            ev.preventDefault()
            ev.stopPropagation()
            dropzone.addClass('dragover')
        }

        dropzone.on('dragleave', (ev)=>{
            ev.preventDefault()
            ev.stopPropagation()
            dropzone.removeClass('dragover')
        })

        dropzone.get(0).ondrop = (ev)=>{
            ev.preventDefault()
            ev.stopPropagation()
            dropzone.removeClass('dragover')
            if(ev.dataTransfer && ev.dataTransfer.items){
                processFile(ev.dataTransfer.items[0].getAsFile())
            } else {
                error('Can\'t process dragged file. Try to click on the button.')
            }
        }

        dropzone.on('click', (ev)=>{
            if(ev.target === fileInput.get(0)){
                return
            }
            fileInput.click()
        })


        svgLoader = $('<div style="visibility: hidden; position: absolute; left: -90000px; bottom: 1px;"></div>')
        $('body').append(svgLoader)

        $(fileInput).on('change', ()=>{
            const file = $(fileInput).get(0).files[0]

            processFile(file)
        })

        for(let t of [outputLua, outputJavascript]){
            if(t){
                t.on('mousedown focus', ()=>{
                    copyToClipboard(t.val())
                })
            }
        }

        resizeCanvas()
        resizeTextareas()
        resetOriginalCanvas()
        reset()

        loadSettings()

        if(settingsContainer){
            buildSettings()
        }        
    }

    function resizeCanvas(){
        for(let c of [canvasOriginal, canvasOriginal2, canvasPoints, canvasLines, canvasTriangles]){
            if(c){
                let w = $(c).parent().width() - parseFloat($(c).css('padding-left').replace('px', '')) - parseFloat($(c).css('padding-right').replace('px', ''))
                let h = parseFloat($(c).parent().parent().css('min-height').replace('px', ''))
                    - parseFloat($(c).css('padding-top').replace('px', ''))
                    - parseFloat($(c).css('padding-bottom').replace('px', ''))
                    - ($(c).parent().find('h4').height() | 0)
                
                c.get(0).width = w
                c.get(0).height = h
                c.width(w)
                c.height(h)
            }
        }
    }

    function resizeTextareas(){
        for(let t of [outputLua, outputJavascript].concat($(outputLuaStormworks.find('textarea')))){
            if(t && t.length){
                t.width(
                    $(t).parent().width() - parseFloat($(t).css('padding-left').replace('px', '')) - parseFloat($(t).css('padding-right').replace('px', ''))
                )

                t.height(Math.min(
                    150,
                    $(t).parent().height() - parseFloat($(t).css('padding-top').replace('px', '')) - parseFloat($(t).css('padding-bottom').replace('px', ''))
                    )
                )
            }
        }
    }

    function resetOriginalCanvas(){
        for(let c of [canvasOriginal, canvasOriginal2]){
            if(c){
                const ctx = c.get(0).getContext('2d');
                ctx.clearRect(0, 0, c.get(0).width, c.get(0).height);
            }
        }
    }

    /*
        reset outputs and canvas, except original canvas
    */
    function reset(){
        for(let c of [canvasPoints, canvasLines, canvasTriangles]){
            if(c){
                const ctx = c.get(0).getContext('2d');
                ctx.clearRect(0, 0, c.get(0).width, c.get(0).height);
            }
        }

        if(outputLua){
            $(outputLua).val('')
        }

        if(outputLuaStormworks !== undefined){
            $(outputLuaStormworks).html('')
        }

        if(outputJavascript){
            $(outputJavascript).val('')
        }

        if(messageContainer){
            $(messageContainer).html('')
        }
    }

    function processFile(file){    
        resetOriginalCanvas()

        if(canvasOriginal){
            const img = new Image()
            img.onload = ()=>{
                loadSVGFileIntoDOM(file).then((svg)=>{

                    transformSVG(svg).then(()=>{
                        try {
                            currentSVGWidth = img.width
                            currentSVGHeight = img.height
                            currentSVGRatio = currentSVGWidth/currentSVGHeight
                            
                            console.log('loaded svg ['+currentSVGWidth+','+currentSVGHeight+'] with ratio ', currentSVGRatio)

                            canvasOriginal.get(0).getContext('2d').drawImage(
                                img,
                                0, 0,
                                currentSVGWidth, currentSVGHeight,
                                0, 0,
                                canvasOriginal.get(0).height*currentSVGRatio, canvasOriginal.get(0).height
                            )

                            svg.find('path').css('fill', '#C62B57')

                            if(canvasOriginal2){
                                const image = new Image(img.width, img.height)

                                image.onload = ()=>{
                                    try {
                                        canvasOriginal2.get(0).getContext('2d').drawImage(
                                            image,
                                            0, 0,
                                            currentSVGWidth, currentSVGHeight,
                                            0, 0,
                                            canvasOriginal2.get(0).height*currentSVGRatio, canvasOriginal2.get(0).height
                                        )
                                    } catch (ex){
                                        warn('Can\'t create SVG view', ex)
                                    } finally {
                                        console.log(image)
                                        $('body').append(image)
                                        processSVG(svg)
                                    }
                                }
                                image.src = 'data:image/svg+xml;base64,' + btoa(new XMLSerializer().serializeToString(svg.get(0)))
                            } else {
                                processSVG(svg)
                            }
                        } catch (ex) {
                            console.error(ex)
                            error('Can\'t create SVG view', ex)
                        }
                    })

                }).catch((ex)=>{
                    console.error(ex)
                    error('Can\'t load SVG', ex)
                })
            }
            img.src = URL.createObjectURL(file)
        }
    }

    function transformSVG(svg){
        return new Promise((fulfill, reject)=>{            
            for(let g of svg.find('g').get()){
                let groupPaths = $(g).find('path');
                
                for(let gp of groupPaths){

                    /* apply transform of the group to the path */
                    for(let t of g.transform.baseVal){
                        gp.transform.baseVal.appendItem(t)
                    }
                    g.transform.baseVal.clear()
                }
            }
            /* allow drawing of svg */
            setTimeout(()=>{
                fulfill()
            }, 1)
        })
    }

    function processSVG(svg){
        try {
            reset()

            let allPaths = []

            for(let g of svg.find('g').get()){
                let groupPaths = $(g).find('path');
                
                for(let gp of groupPaths){

                    /* apply transform of the group to the path */
                    for(let t of g.transform.baseVal){
                        gp.transform.baseVal.appendItem(t)
                    }
                    g.transform.baseVal.clear()
                    /*for(let t of g.transform.animVal){
                        gp.transform.animVal.appendItem(t)
                    }*/
                    allPaths.push(gp)
                }
            }

            allPaths = allPaths.concat(svg.find('> path').get())

            if(allPaths.length == 0){
                error('No path found in SVG')
                return
            }

            let offcanvasPoints = document.createElement('canvas')
            offcanvasPoints.width = currentSVGWidth
            offcanvasPoints.height = currentSVGHeight
            //$('body').append(offcanvasPoints)

            let offcanvasLines = document.createElement('canvas')
            offcanvasLines.width = currentSVGWidth
            offcanvasLines.height = currentSVGHeight
            //$('body').append(offcanvasLines)

            let offcanvasTriangles = document.createElement('canvas')
            offcanvasTriangles.width = currentSVGWidth
            offcanvasTriangles.height = currentSVGHeight
            //$('body').append(offcanvasTriangles)

            let allPoints = []
            let allLines = []
            let allTriangles = []

            for(let p of allPaths){
                let points = generatePointsForSVGPath(p)
                allPoints = allPoints.concat(points)

                let lines = generateLinesForPoints(points)
                allLines = allLines.concat(lines)

                let triangles = generateTriangleMeshForPoints(points)

                allTriangles= allTriangles.concat(triangles)
            }

            if(canvasPoints){
                drawPoints($(offcanvasPoints), allPoints)

                canvasPoints.get(0).getContext('2d').drawImage(offcanvasPoints,
                    0, 0,
                    offcanvasPoints.width, offcanvasPoints.height,
                    0, 0,
                    canvasPoints.get(0).height * currentSVGRatio, canvasPoints.get(0).height
                )
            }

            if(canvasLines){
                drawLines($(offcanvasLines), allLines)

                canvasLines.get(0).getContext('2d').drawImage(offcanvasLines,
                    0, 0,
                    offcanvasLines.width, offcanvasLines.height,
                    0, 0,
                    canvasLines.get(0).height * currentSVGRatio, canvasLines.get(0).height
                )
            }

            if(canvasTriangles){
                drawTriangles($(offcanvasTriangles), allTriangles)

                canvasTriangles.get(0).getContext('2d').drawImage(offcanvasTriangles,
                    0, 0,
                    offcanvasTriangles.width, offcanvasTriangles.height,
                    0, 0,
                    canvasTriangles.get(0).height * currentSVGRatio, canvasTriangles.get(0).height
                )
            }

            if(outputLua){
                $(outputLua).val(
                    generateLuaCode(allTriangles)
                )
            }

            if(outputLuaStormworks){
                let scripts = generateLuaScriptsStormworks(allTriangles)
                for(let s of scripts){
                    let t = $('<textarea></textarea')
                    t.val(s)
                    
                    t.on('mousedown focus', ()=>{
                        copyToClipboard(t.val())
                    })
                    t.attr('readonly', 'true')
                    $(outputLuaStormworks).append(t)
                }
            }

            if(outputJavascript){
                $(outputJavascript).val(
                    generateJavascriptCode(allTriangles)
                )
            }

            success('Generated ' + allTriangles.length + ' triangles')
        } catch (ex){
            console.error(ex)
            error('Can\'t process SVG', ex)
        }
    }

    function loadSVGFileIntoDOM(file){
        return new Promise((fulfill, reject)=>{
            svgLoader.html('')

            xhr = new XMLHttpRequest()
            xhr.open("GET",URL.createObjectURL(file))
            xhr.overrideMimeType("image/svg+xml")
            xhr.onreadystatechange = function(){
                if(xhr.readyState === 4) {
                    if(xhr.status == 200){
                        let svg = xhr.responseXML.documentElement
                        svgLoader.append(svg)
                        currentSVG = $(svg)

                        /* allow drawing of svg */
                        setTimeout(()=>{
                            fulfill($(svg))
                        }, 1)
                    } else {
                        console.error('cannot load svg:', xhr.status, xhr.statusText, xhr.responseText)
                        reject()
                    }
                }
            }
            xhr.send("")
        })
    }
    
    /* 
        Source:
        https://stackoverflow.com/questions/53393966/convert-svg-path-to-polygon-coordinates
    */
    function generatePointsForSVGPath(path){
        let len = path.getTotalLength()
        let points = []

        let xMax = 0
        let xMin = 10000000000000000
        let yMax = 0
        let yMin = 10000000000000000

        const NUM_POINTS = getSetting('points_per_svg_path')
        for (let i=0; i < NUM_POINTS; i++) {
            try {
                let pt = path.getPointAtLength(i * len / (NUM_POINTS-1))
                if(path.transform.baseVal.length > 0){
                    let matrix = path.transform.baseVal.consolidate().matrix
                    pt = pt.matrixTransform(matrix)
                }
                points.push(pt.x)
                points.push(pt.y)

                if(pt.x > xMax){
                    xMax = pt.x
                }
                if(pt.x < xMin){
                    xMin = pt.x
                }
                if(pt.y > yMax){
                    yMax = pt.y
                }
                if(pt.y < yMin){
                    yMin = pt.y
                }
            } catch (ex){
                console.error('error generating points:', ex, i * len / (NUM_POINTS-1))
                continue
            }
        }

        return points
    }
    

    /*
        points = [x1,y1, x2,y2, x3,y3, x4, y4]
    */
    function generateLinesForPoints(points){
        
        let lines = []

        let skipped = 0

        for(let i=0; i<points.length-3; i+=4){
            // read points
            let x1 = points[i]
            let y1 = points[i+1]
            let x2 = points[i+2]
            let y2 = points[i+3]

            // check if all coordinates are valid
            let valid=true
            if(typeof x1 !== 'number'){
                valid = false
                console.log('@ line['+i+']: invalid x1=',x1)
            }            
            if(typeof y1 !== 'number'){
                valid = false
                console.log('@ line['+i+']: invalid y1=',y1)
            }
            if(typeof x2 !== 'number'){
                valid = false
                console.log('@ line['+i+']: invalid x2=',x2)
            }            
            if(typeof y2 !== 'number'){
                valid = false
                console.log('@ line['+i+']: invalid y2=',y2)
            }



            if(!valid){
                skipped++
                continue
            }

            lines.push({x1: x1, y1: y1, x2: x2, y2: y2})
        }

        if(skipped > 0){
            console.warn('skipped ' + skipped + ' invalid lines' + (skipped > 1 ? 's' : ''), 'skipped lines are irrelevant!')
        }

        return lines
    }

    /*
        points = [x1,y1, x2,y2, x3,y3]
    */
    function generateTriangleMeshForPoints(points){
        let vertices = earcut(points)

        let triangles = []

        let skipped = 0

        for(let i=0; i<vertices.length-2; i+=3){
            // read vertices
            let v1 = vertices[i]
            let v2 = vertices[i+1]
            let v3 = vertices[i+2]

            // read points
            let x1 = points[v1*2]
            let y1 = points[v1*2+1]
            let x2 = points[v2*2]
            let y2 = points[v2*2+1]
            let x3 = points[v3*2]
            let y3 = points[v3*2+1]

            // check if all coordinates are valid
            let valid=true
            if(typeof x1 !== 'number'){
                valid = false
                console.log('@ triangle['+v1+','+v2+','+v3+']: invalid x1=',x1)
            }            
            if(typeof y1 !== 'number'){
                valid = false
                console.log('@ triangle['+v1+','+v2+','+v3+']: invalid y1=',y1)
            }
            if(typeof x2 !== 'number'){
                valid = false
                console.log('@ triangle['+v1+','+v2+','+v3+']: invalid x2=',x2)
            }
            if(typeof y2 !== 'number'){
                valid = false
                console.log('@ triangle['+v1+','+v2+','+v3+']: invalid y2=',y2)
            }
            if(typeof x3 !== 'number'){
                valid = false
                console.log('@ triangle['+v1+','+v2+','+v3+']: invalid x3=',x3)
            }
            if(typeof y3 !== 'number'){
                valid = false
                console.log('@ triangle['+v1+','+v2+','+v3+']: invalid y3=',y3)
            }

            if(!valid){
                skipped++
                continue
            }

            // cut decimals and push to list
            let t = {
                x1: cutDecimals(x1),
                y1: cutDecimals(y1),
                x2: cutDecimals(x2),
                y2: cutDecimals(y2),
                x3: cutDecimals(x3),
                y3: cutDecimals(y3)
            }
            triangles.push(t)
        }

        if(skipped > 0){
            warn('skipped ' + skipped + ' invalid triangle' + (skipped > 1 ? 's' : ''), '1 or two skipped triangles is considered to be normal')
        }

        return triangles
    }

    /*
        points = [x1,y1, x2,y2, x3,y3]
    */
    function drawPoints(canvas, points){
        ctx=canvas.get(0).getContext('2d')
        ctx.fillStyle = hexToRGB(settings.color.value)

        for(let i=0; i < points.length-1; i=i+2){
            ctx.lineWidth = 0
            ctx.fillRect(points[i], points[i+1], 2, 2)
        }
    }

    /*
        triangles = [{x1: 10, y1: 10, x2: 20, y2: 20, x3: 10, y3: 20}, {...}]
    */
    function drawLines(canvas, lines){
        ctx=canvas.get(0).getContext('2d')
        ctx.strokeStyle = hexToRGB(settings.color.value)

        for(let l of lines){
            ctx.lineWidth = 2
            ctx.beginPath()
            ctx.moveTo(l.x1, l.y1)
            ctx.lineTo(l.x2, l.y2)
            ctx.stroke()
            ctx.closePath()
        }
    }

    /*
        triangles = [{x1: 10, y1: 10, x2: 20, y2: 20, x3: 10, y3: 20}, {...}]
    */
    function drawTriangles(canvas, triangles){
        ctx=canvas.get(0).getContext('2d')
        ctx.fillStyle = hexToRGB(settings.color.value).substring(0, hexToRGB(settings.color.value).length - 1) + ',0.5)'

        for(let i=0; i<5; i++){// repeat 5 times to get rid of ugly white stripes in canvas
            for(let t of triangles){
                ctx.lineWidth = 0
                ctx.beginPath()
                ctx.moveTo(t.x1, t.y1)
                ctx.lineTo(t.x2, t.y2)
                ctx.lineTo(t.x3, t.y3)
                ctx.lineTo(t.x1, t.y1)
                ctx.fill()
                ctx.closePath()
            }
        }
    }

    function generateLuaCode(triangles){
        let luaCodeTriangleArray = "{"

        for(let i=0; i<triangles.length; i++){
            const t = triangles[i]
            luaCodeTriangleArray += '{' + [t.x1,t.y1,t.x2,t.y2,t.x3,t.y3].join(',') + '}'

            if(i < triangles.length-1){
                luaCodeTriangleArray+=','
            }
        }

        luaCodeTriangleArray += "}"

        return luaCodeTriangleArray
    }

    function generateLuaScriptsStormworks(triangles){
        const NL = '\n'

        let drawCode = NL + NL + 'function compileTriangles(cx,cy,size)' + NL
            + ' for i=1,#TS do' + NL
            + '     TS[i][1] = TS[i][1]*size+cx' + NL
            + '     TS[i][2] = TS[i][2]*size+cy' + NL
            + NL
            + '     TS[i][3] = TS[i][3]*size+cx' + NL
            + '     TS[i][4] = TS[i][4]*size+cy' + NL
            + NL
            + '     TS[i][5] = TS[i][5]*size+cx' + NL
            + '     TS[i][6] = TS[i][6]*size+cy' + NL
            + ' end' + NL
            + 'end' + NL
            + NL
            + 'compileTriangles(0,0,' + (getSetting('output_image_height')/currentSVGHeight) + ')' + NL
            + NL
            + 'function onDraw()' + NL
            + ' screen.setColor' + hexToRGB(settings.color.value).replace('rgb', '') + NL
            + ' for _,t in pairs(TS) do' + NL
            + '     screen.drawTriangleF(t[1],t[2],t[3],t[4],t[5],t[6])' + NL
            + ' end' + NL
            + 'end' + NL

        let availableLengthFroTriangles = getSetting('max_chars_per_lua_script') - drawCode.length - 'TS='.length

        let scripts = []
        let currentTriangleIndex = 0
        while(currentTriangleIndex < triangles.length){
            let myTriangleArray = "{"
            while(true && currentTriangleIndex < triangles.length){
                let next = triangleToString(triangles[currentTriangleIndex])+','
                if(myTriangleArray.length + next.length < availableLengthFroTriangles){
                    myTriangleArray += next
                    currentTriangleIndex++
                } else {
                    break
                }
            }
            scripts.push(('TS=' + myTriangleArray + '}').replace('},}', '}}') + drawCode)
        }

        function triangleToString(t){
            return '{' + [t.x1,t.y1,t.x2,t.y2,t.x3,t.y3].join(',') + '}'
        }

        return scripts
    }

    function generateJavascriptCode(triangles){
        let javascriptCodeTriangleArray = "["

        for(let i=0; i<triangles.length; i++){
            const t = triangles[i]
            javascriptCodeTriangleArray += '[' + [t.x1,t.y1,t.x2,t.y2,t.x3,t.y3].join(',') + ']'

            if(i < triangles.length-1){
                javascriptCodeTriangleArray+=','
            }
        }

        javascriptCodeTriangleArray += "]"

        return javascriptCodeTriangleArray
    }

    /*
        cutDecimals(0.12345, 3) = 0.123    
    */
    function cutDecimals(number){
        let decimalsToStay = getSetting('decimals_to_stay')
        if(decimalsToStay == 0){
            return Math.floor(number)
        }
        return Math.floor(number * (10**decimalsToStay)) / (10**decimalsToStay)
    }

    function error(message, details){
        $(messageContainer).append('<div class="message message_error"><strong>Error:</strong>&nbsp;' + message + (details !== null && details !== undefined ? ('<span class="i">i</span><div class="details">' + details.toString() + '</div>') : '') + '</div>')
    }

    function warn(message, details){
        $(messageContainer).append('<div class="message message_warning"><strong>Warning:</strong>&nbsp;' + message + (details !== null && details !== undefined ? ('<span class="i">i</span><div class="details">' + details.toString() + '</div>') : '')  + '</div>')
    }

    function success(message, details){
        $(messageContainer).append('<div class="message message_success"><strong>Success:</strong>&nbsp;' + message + (details !== null && details !== undefined ? ('<span class="i">i</span><div class="details">' + details.toString() + '</div>') : '')  + '</div>')
    }

    function buildSettings(){
        $(settingsContainer).html('')
        $(settingsContainer).append( 
            $('<span class="button">Reset Settings</span>').click(()=>{
                resetSettings()
            })
        )

        for(let k in settings){
            const s = settings[k]
            let cont = $('<div class="setting"><div class="left"><label>' + s.label + '</label><span>' + s.hint + '</span></div><div class="right"></div></div>')
            
            let input
            switch(s.type){
                case 'number': {
                    input = $('<input type="number">')
                    .val(s.value)
                    .on('change', ()=>{
                        s.value = parseInt(input.val())
                        if(typeof s.min === 'number' && s.value < s.min){
                            s.value = s.min
                        }
                        if(typeof s.max === 'number' &&  s.value > s.max){
                            s.value = s.max
                        }
                        input.val(s.value)
                        saveSettings()
                        if(currentSVG){
                            processSVG(currentSVG)
                        }
                    })
                    if(typeof s.min === 'number'){
                        input.attr('min', s.min)
                    }
                    if(typeof s.max === 'number'){
                        input.attr('max', s.max)
                    }
                }; break;
                case 'boolean': {
                    input = $('<input type="checkbox">').prop('checked', s.value).on('change', ()=>{
                        s.value = input.prop('checked')
                        saveSettings()
                        if(currentSVG){
                            processSVG(currentSVG)
                        }
                    })
                }; break;
                default: {
                    if(s.type === 'color'){
                        input = $('<input type="color">').val(s.value).on('change', ()=>{
                            s.value = input.val()
                            saveSettings()
                            if(currentSVG){
                                processSVG(currentSVG)
                            }
                        })
                    } else {
                        console.error('setting type not supported:', typeof s.value, k)
                    }
                }
            }

            cont.find('.right').append(input)

            $(settingsContainer).append(cont)
        }
    }

    function getSetting(name){
        if(settings[name] && settings[name].value !== undefined){
            return settings[name].value
        } else {
            console.error('could not find setting['+name+']')
            return undefined
        }
    }

    function loadSettings(){
        try {
            let oldSettings = localStorage.getItem('settings')
            try {
                oldSettings = JSON.parse(oldSettings)            
            } catch(thrown){}

            for(let k in settings){
                if(oldSettings && oldSettings[k] && typeof settings[k].default === typeof oldSettings[k].value){
                    settings[k].value = oldSettings[k].value
                } else {
                    settings[k].value = settings[k].default
                }
            }
        } catch (ex){
            console.warn('cannot load oldSettings', ex)
        }
    }

    function saveSettings(){
        localStorage.setItem('settings', JSON.stringify(settings))
    }

    function resetSettings(){
        for(let k in settings){
            settings[k].value = settings[k].default
        }
        saveSettings()
        buildSettings()
        if(currentSVG){
            processSVG(currentSVG)
        }
    }

    function copyToClipboard(text){
        if(!text){
            return
        }
        try {
            const el = $('<textarea></textarea>');
            el.val(text)
            $('body').append(el)
            el.get(0).select()
            document.execCommand('copy')
            el.remove()
            
            let hint = $('<span style="position: fixed; bottom: 40px; left: 50%; transform: translateX(-50%); padding: 10px 20px; background: #000; color: #fff; font-size: 16px; border-radius: 10000px;">Copied to Clipboard</span>')
            $('body').append(hint)
            setTimeout(()=>{
                hint.fadeOut()
            }, 2000)
        } catch(ex){
            console.error('cannot copy to clipboard', ex)
        }
    }

    function hexToRGB(hex) {
      // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
      var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
      });

      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      console.log(result)
      return result ? ('rgb(' + parseInt(result[1], 16) + ',' + parseInt(result[2], 16) + ',' + parseInt(result[3], 16) + ')') : null
    }

    return {

    }
})({
    dropzone: $('#dropzone'),
    canvasOriginal: $('#canvas-original'),
    canvasOriginal2: $('#canvas-original-2'),
    canvasPoints: $('#canvas-points'),
    canvasLines: $('#canvas-lines'),
    canvasTriangles: $('#canvas-triangles'),
    outputJavascript: $('#output-javascript'),
    outputLua: $('#output-lua'),
    outputLuaStormworks: $('#output-lua-stormworks'),
    settingsContainer: $('#settings-container'),
    messageContainer: $('#message-container')
})
